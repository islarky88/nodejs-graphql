const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const postSchema = new Schema({
    title: {
        type: String,
        required: true

    },
    description: {
        type: String,
        required: true

    },
    price: {
        type: Number,
        required: true

    },
    date: {
        type: Date,
        required: true

    }
})

let Post = module.exports = mongoose.model('posts', postSchema);