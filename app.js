const express = require("express");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");
const graphqlHttp = require("express-graphql");
const { buildSchema } = require("graphql");

const Post = require("./models/posts");

const app = express();

mongoose.connect("mongodb://localhost/graphql", { useNewUrlParser: true });
let db = mongoose.connection;

db.once("open", function() {
  console.log("connected to mongo db");
});

const events = [];

app.use(bodyParser.json());

app.use(
  "/api",
  graphqlHttp({
    schema: buildSchema(`
    
        type Event {
            _id: ID!
            title: String!
            description: String!
            price: Float!
            date: String!
        }

        input MutateInput {
            title: String!
            description: String!
            price: Float!
            date: String!
        }
        type rootQuery { 
            fetch: [Event!]!
        }

        type rootMutation {
            mutate(data: MutateInput): Event

        }
        

        
        schema {
            query: rootQuery
            mutation: rootMutation
        }
    `),
    rootValue: {
      fetch: () => {
        return Post.find()
          .then(results => {
            return results.map(result => {
              return { ...result._doc };
            });
          })
          .catch(err => {
            console.log(err);
            throw err;
          });
      },
      mutate: args => {
        // const event = {
        //     _id: Math.random().toString(),
        //     title: args.data.title,
        //     description: args.data.description,
        //     price: +args.data.price,
        //     date: args.data.date
        // };

        const post = new Post({
          title: args.data.title,
          description: args.data.description,
          price: +args.data.price,
          date: new Date().toISOString()
        });
        return post
          .save()
          .then(result => {
            console.log(result);
            return { ...result._doc };
          })
          .catch(err => {
            console.log(err);
            throw err;
          });
        return event;
      }
    },

    graphiql: true
  })
);

// app.get('/', (req, res, next) => {
//     res.send('Hello World!');
// });

app.listen(3001);
